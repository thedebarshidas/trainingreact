<h1>React Training</h1>
<h2> by <i> Debarshi Das </i></h2>

<h3>About</h3>
<p>
This repo contains all the code files for the Intern Training Tech 2: Javascript and React week(s).

Each days code is categorically organsied in separate branches. Each branch corresponds to a day. Their respective README files contain the synopsis and the task details.
</p>

<h3>Clone & Run</h3>
<p>

<h4>To clone:</h4>

`> git clone https://gitlab.com/thedebarshidas/trainingreact.git `

<h4>To go to each days work on their corresponding branches:</h4>

`> git checkout day1 `

<h4>To install the dependencies & view the project live:</h4>

`> npm i `

`> npm start `
</p>

<p>
<h3>Contact</h3>
email: debarshi.d.das@pwc.com
</p>
